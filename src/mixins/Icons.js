import {
  matAdd,
  matDelete,
  matLightbulb,
  matEmojiEvents,
  matGavel,
  matFileUpload,
  matPolicy,
  matEmail,
  matPublic,
  matStyle,
  matTranslate,
  matSend,
  matWest,
  matHourglassBottom,
  matSettings,
  matInfo,
  matWarning,
  matDangerous,
  matDescription,
  matGroup,
  matPerson,
  matGroups,
  matEmojiObjects,
  matMovie,
  matChevronRight,
  matMarkEmailRead,
  matArrowBack,
  matLanguage,
  matUpload,
  matMoreHoriz,
  matPendingActions,
  matAutoFixHigh,
  matShare,
  matSouth,
  matLogout,
  matEdit,
  matPlayArrow,
  matDownload,
  matCheck,
  matNewspaper,
} from "@quasar/extras/material-icons";

import { fabLinkedinIn, farEnvelope } from "@quasar/extras/fontawesome-v5";

export default {
  created() {
    this.matNewspaper = matNewspaper;
    this.matPlayArrow = matPlayArrow;
    this.matCheck = matCheck;
    this.matDownload = matDownload;
    this.matEdit = matEdit;
    this.matLogout = matLogout;
    this.matSouth = matSouth;
    this.matShare = matShare;
    this.matAutoFixHigh = matAutoFixHigh;
    this.matPendingActions = matPendingActions;
    this.fabLinkedinIn = fabLinkedinIn;
    this.farEnvelope = farEnvelope;
    this.matMoreHoriz = matMoreHoriz;
    this.matUpload = matUpload;
    this.matLanguage = matLanguage;
    this.matChevronRight = matChevronRight;
    this.matMovie = matMovie;
    this.matEmojiObjects = matEmojiObjects;
    this.matGroup = matGroup;
    this.matPerson = matPerson;
    this.matGroups = matGroups;
    this.matDescription = matDescription;
    this.matInfo = matInfo;
    this.matWarning = matWarning;
    this.matDangerous = matDangerous;
    this.matHourglassBottom = matHourglassBottom;
    this.matSettings = matSettings;
    this.matAdd = matAdd;
    this.matDelete = matDelete;
    this.matPolicy = matPolicy;
    this.matEmail = matEmail;
    this.matPublic = matPublic;
    this.matStyle = matStyle;
    this.matTranslate = matTranslate;
    this.matSend = matSend;
    this.matWest = matWest;
    this.matAdd = matAdd;
    this.matLightbulb = matLightbulb;
    this.matEmojiEvents = matEmojiEvents;
    this.matGavel = matGavel;
    this.matFileUpload = matFileUpload;
    this.matMarkEmailRead = matMarkEmailRead;
    this.matArrowBack = matArrowBack;
  },
};
